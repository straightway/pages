<!DOCTYPE html>
<html>

  <head>
    <script src="/jquery-3.4.1.min.js"></script>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,maximum-scale=2">
    <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/style.css">
    <script>
      function scrollToBottom() {
        $('html, body').scrollTop($(document).height() - $(window).height());
      }
    </script>

<!-- Begin Jekyll SEO tag v2.7.1 -->
<title>Straightway ODO Distributed Online</title>
<meta name="generator" content="Jekyll v4.2.0" />
<meta property="og:title" content="Straightway ODO Distributed Online" />
<meta property="og:locale" content="en_US" />
<meta property="og:site_name" content="Straightway ODO Distributed Online" />
<meta name="twitter:card" content="summary" />
<meta property="twitter:title" content="Straightway ODO Distributed Online" />
<script type="application/ld+json">
{"@type":"WebPage","url":"/Design/odoArchitecture.html","headline":"Straightway ODO Distributed Online","@context":"https://schema.org"}</script>
<!-- End Jekyll SEO tag -->

  </head>

  <body>

    <!-- HEADER -->
    <div id="header_wrap" class="outer">
        <header class="inner" style="padding:0">
          <table>
            <tr>
              <td><h1 id="project_title">Straightway ODO</h1></td>
              <td><a href="/odo">home</a></td>
              <td><a href="https://codeberg.org/straightway/odo">repo</a></td>
              <td><a href="https://codeberg.org/straightway/odo/issues">issues</a></td>
              <td><a href="api">API</a></td>
            </tr>
          </table>
        </header>
    </div>

    <!-- MAIN CONTENT -->
    <div id="main_content_wrap" class="outer">
      <section id="main_content" class="inner">
          <h1 id="odo-architectural-overview">ODO Architectural Overview</h1>

<p>ODO is a system running on mobile or desktop devices hosting ODO-apps. These apps can be uploaded to and downloaded from serverless distributed storage clouds. All data stored within these clouds is strongly encrypted. Currently, the only cloud implementation uses <a href="https://ipfs.io/">IPFS</a>.</p>

<p><img src="TopLevelArchitecture.png" alt="architecture overview" /></p>

<p>The following text outlines the target to reach with ODO from a top level perspective. Parts that are not yet implemented are marked <em>italic</em>.</p>

<h2 id="odo-apps">ODO-Apps</h2>

<p>An ODO-app consists basically of an app manifest containing meta info and a jar archive with the app binaries. It may contain more optional files, e.g. an icon image.</p>

<p><em>The app code has restricted dependencies, i.e. it may only depend on the API interfaces provided by ODO. Especially direct access to I/O,  UI and other system resources is blocked and only possible in a controlled and trustworthy way through the ODO API. This way, the user may grant privileges to ODO-apps, e.g. read or write access to local storage or cloud storage, using certain user identities (see below) for communication or presenting a UI to the user.</em></p>

<h2 id="odo-identities">ODO Identities</h2>

<p>In ODO, a user may create any number of identities. An identity is basically an asymmetric crypto key pair, plus additional information such as name and image. So an identity may be used to digitally decrypt or sign content.</p>

<p><em>The user may grant ODO-apps access to identities for contact sharing and secure communication.</em></p>

<h2 id="odo-contacts">ODO contacts</h2>

<p><em>Users may strip identities from their private key and share them with other users as contacts. Users may grant access to all or only specifc contacts to ODO-apps. They may also grant the privilege to add or remove contacts.</em></p>

<h2 id="cloud-storage">Cloud storage</h2>

<p>The cloud storage in ODO is closely attached to the mechanisms in IPFS, which is the basis for the currently only cloud implementation.</p>

<p>Users may grant cloud access for ODO-apps. Because the cloud is the medium ODO-apps use to etablish communication, it is crucial to design cloud access in a way that makes it easy to write privacy-friendly ODO-apps.</p>

<p>On the other hand, leaking private information should be made as hard as possible. <em>So direct communication must be explicitly permitted by the user.</em></p>

<p>Read and write access to cloud storage is realized by means of cryptography. Content may be encrypted using a contact’s public key to grant access only to this contact. Or it may be encrypted using a symmetric key to grant access to groups of contacts.</p>

<p>Write access to cloud storage is only possible through owned <em>cloud roots</em> (in IPFS published keys). Content may be stored to the cloud and then be published under a cloud root.</p>

<p>Cloud content is identified by Uniform Resouce Identifiers (URI). An URI may contain all information required to read the specified data, e.g. the cloud root and the decryption key. Or it may e.g. omit the decryption key, so that the user must provide it (e.g. by providing a private key of a contact).</p>

<h2 id="direct-communication">Direct communication</h2>

<p>Cloud storage is good for a follower-based communication model with polling, high latency and relatively static content. But it does not fit well for push notifications or streaming. <em>Here we have to other means, that may be granted to ODO-apps as privileges:</em></p>

<ul>
  <li><em>pubsub: A simple publisher/subscriber pattern. An app may subscribe to a channel and others may send on this channel. Channel events are only received by ODO-apps if they are running and online. This allows direct communication between ODO-app instances.</em></li>
  <li><em>Direct connection between peers using <a href="https://www.torproject.org/">Tor</a> .</em></li>
</ul>

<h2 id="api">API</h2>

<p>The ODO API is the main interface used by ODO-apps. It is the only way for them to interact with the user or the real world. The API checks privilages and makes sure that apps only do what they are allowed to do. It offers functionality in the following areas:</p>

<ul>
  <li>User interaction</li>
  <li><em>Networking</em></li>
  <li><em>Storage</em></li>
  <li><em>Identity management</em></li>
  <li><em>Contact management</em></li>
</ul>

<p>It may be extended later to provide means for:</p>
<ul>
  <li><em>Payment</em></li>
  <li><em>Cross app communication</em></li>
  <li>…</li>
</ul>

<p><em>The API is designed with downwards compatibility in mind, so that existing apps don’t have to be adapted when new API version occur.</em></p>

<p>Currently, ODO is almost entirely implemented in <a href="https://kotlinlang.org">kotlin</a>, and also the API is a kotlin API. I offers suspendable functions wherever reasonable, so that ODO-apps are normally asynchronous and multi-threaded.</p>

<p><em>However, one of the targets of ODO is making it as easy as possible to write apps. For this reason, it might be useful to support a development environment, that ist either interpreted or compiled just in time, to relieve developers from having to set up a build system. Candidates may be <a href="https://python.org">python</a> or kotlin script (like in gradle).</em></p>

<h2 id="whistle-blowing">Whistle blowing</h2>

<p><em>As ODO allows creating apps with strong technical privacy guarantees, it might be appealing for criminals to use such ODO-apps for distribution of illegal content or planning of crimes. As providing a master key for authorities would be a threat to the privacy of the very vast majority of respectable users, ODO provides general means for whistle blowers to report criminal content. The mechanism for blowing the whistle must be anonymous, easy to use, and as difficult as possible to detect so as to protect potential whistleblowers.</em></p>

<p><em>One way to realize this would be to send encrypted content on a regular basis via a pubsub mechanism. If the user does not report any criminal content, it simply contains random content. If criminal content is reported, it may contain an encrypted URI.</em></p>

<p><em>Please notice, that this must be part of the ODO launcher, not of the ODO-apps, so that all ODO-apps always support whistle blowing.</em></p>

      </section>
    </div>

    <!-- FOOTER  -->
    <div id="footer_wrap" class="outer">
      <footer class="inner">
        <tr>
          <td><a onclick="showImprint(); scrollToBottom();" style="cursor:pointer;">Imprint</a></td>
          <td><a href="/privacy.html">Privacy</a></td>
        </tr>
        <div id="imprint" style="display:none">
          <img src="/img/impressum.png" alt="Impressum"/>
        </div>
        <script>
        function showImprint() {
          var x = document.getElementById('imprint');
          if (x.style.display === 'none') {
            x.style.display = 'block';
          } else {
            x.style.display = 'none';
          }
        }
        </script>
      </footer>
    </div>
  </body>
</html>
